#!/usr/bin/env python3

import copy

FLOOR = "."
EMPTY = "L"
OCCUPIED = "#"


def part1(input_grid):
    num_seat_changes, new_grid = do_part1_seating(input_grid)
    while num_seat_changes != 0:
        num_seat_changes, new_grid = do_part1_seating(new_grid)

    return count_total_seats_occupied(new_grid)


def do_part1_seating(orig_grid):
    new_grid = copy.deepcopy(orig_grid)
    num_seat_changes = 0

    for row_num, row in enumerate(orig_grid):
        for column_num, seat in enumerate(row):
            if seat == EMPTY and count_immediate_adjacent_occupied(orig_grid, row_num, column_num) == 0:
                new_grid[row_num][column_num] = OCCUPIED
                num_seat_changes += 1
            elif seat == OCCUPIED and count_immediate_adjacent_occupied(orig_grid, row_num, column_num) >= 4:
                new_grid[row_num][column_num] = EMPTY
                num_seat_changes += 1
            else:
                new_grid[row_num][column_num] = seat

    return num_seat_changes, new_grid


def count_immediate_adjacent_occupied(grid, row, col):
    num_adjacent_occupied = 0

    # check top left
    if row - 1 >= 0 and col - 1 >= 0 and grid[row - 1][col - 1] == OCCUPIED:
        num_adjacent_occupied += 1

    # check top
    if row - 1 >= 0 and grid[row-1][col] == OCCUPIED:
        num_adjacent_occupied += 1

    # check top right
    if row - 1 >= 0 and col + 1 < len(grid[row]) and grid[row - 1][col + 1] == OCCUPIED:
        num_adjacent_occupied += 1

    # check left
    if col - 1 >= 0 and grid[row][col - 1] == OCCUPIED:
        num_adjacent_occupied += 1

    # check right
    if col + 1 < len(grid[row]) and grid[row][col + 1] == OCCUPIED:
        num_adjacent_occupied += 1

    # check bottom left
    if row + 1 < len(grid) and col - 1 >= 0 and grid[row + 1][col - 1] == OCCUPIED:
        num_adjacent_occupied += 1

    # check bottom
    if row + 1 < len(grid) and grid[row+1][col] == OCCUPIED:
        num_adjacent_occupied += 1

    # check bottom right
    if row + 1 < len(grid) and col + 1 < len(grid[row]) and grid[row + 1][col + 1] == OCCUPIED:
        num_adjacent_occupied += 1

    return num_adjacent_occupied


def part2(input_grid):
    num_seat_changes, new_grid = do_part2_seating(input_grid)
    while num_seat_changes != 0:
        num_seat_changes, new_grid = do_part2_seating(new_grid)

    return count_total_seats_occupied(new_grid)


def do_part2_seating(orig_grid):
    new_grid = copy.deepcopy(orig_grid)
    num_seat_changes = 0

    for row_num, row in enumerate(orig_grid):
        for column_num, seat in enumerate(row):
            if seat == EMPTY and count_all_adjacent_occupied(orig_grid, row_num, column_num) == 0:
                new_grid[row_num][column_num] = OCCUPIED
                num_seat_changes += 1
            elif seat == OCCUPIED and count_all_adjacent_occupied(orig_grid, row_num, column_num) >= 5:
                new_grid[row_num][column_num] = EMPTY
                num_seat_changes += 1
            else:
                new_grid[row_num][column_num] = seat

    return num_seat_changes, new_grid


def count_all_adjacent_occupied(grid, row, col):
    num_adjacent_occupied = 0

    # check top left diagonal
    r = row - 1
    c = col - 1
    while 0 <= r < row and 0 <= c < col:
        if grid[r][c] == EMPTY:
            break
        if grid[r][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r -= 1
        c -= 1

    # check all top
    r = row - 1
    while 0 <= r < row:
        if grid[r][col] == EMPTY:
            break
        if grid[r][col] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r -= 1

    # check top right diagonal
    r = row - 1
    c = col + 1
    while 0 <= r < row and col <= c < len(grid[row]):
        if grid[r][c] == EMPTY:
            break
        if grid[r][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r -= 1
        c += 1

    # check all left
    c = col - 1
    while 0 <= c < col:
        if grid[row][c] == EMPTY:
            break
        if grid[row][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        c -= 1

    # check all right
    c = col + 1
    while col <= c < len(grid[row]):
        if grid[row][c] == EMPTY:
            break
        if grid[row][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        c += 1

    # check bottom left diagonal
    r = row + 1
    c = col - 1
    while row <= r < len(grid) and 0 <= c < col:
        if grid[r][c] == EMPTY:
            break
        if grid[r][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r += 1
        c -= 1

    # check all bottom
    r = row + 1
    while row <= r < len(grid):
        if grid[r][col] == EMPTY:
            break
        if grid[r][col] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r += 1

    # check bottom right diagonal
    r = row + 1
    c = col + 1
    while row <= r < len(grid) and col <= c < len(grid[row]):
        if grid[r][c] == EMPTY:
            break
        if grid[r][c] == OCCUPIED:
            num_adjacent_occupied += 1
            break
        r += 1
        c += 1

    return num_adjacent_occupied


def count_total_seats_occupied(grid_to_count):
    num_seats_occupied = sum(r.count(OCCUPIED) for r in grid_to_count)
    return num_seats_occupied


if __name__ == '__main__':
    with open("../resources/day_11_input.txt", mode="r") as file:
        lines = list(file)

    grid = []
    for line in lines:
        row = list(line)
        if "\n" in row:
            row.remove("\n")
        grid.append(row)

    print("Part 1 = {}".format(part1(grid)))
    print("Part 2 = {}".format(part2(grid)))
