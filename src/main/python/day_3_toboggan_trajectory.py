#!/usr/bin/env python3


def part1(input_lines):
    full_map = load_map(input_lines)
    num_trees = traverse(full_map, 3, 1)
    print("Part 1 = {} trees encountered".format(num_trees))
    return num_trees


def load_map(input_lines):
    full_map = []
    for line in input_lines:
        row = list(line)
        if '\n' in row:
            row.remove('\n')
        full_map.append(row)
    return full_map


def traverse(full_map, num_right, num_down):
    num_trees = 0
    current_column = 0
    current_row = 0

    for row in full_map:
        if current_column > len(row) - 1:
            # Reached end of input line - wrap around (assuming same pattern continues on this line indefinitely)
            current_column = current_column - len(row)
        if current_row > len(full_map) - 1:
            # We're at the bottom row, so can't go down further - break to avoid index out of bounds error on next line
            break
        curr_location = full_map[current_row][current_column]
        if curr_location == '#':
            # print("Tree found at [{}][{}] (during {}, {})".format(current_row, current_column, num_right, num_down))
            num_trees += 1

        current_column += num_right
        current_row += num_down

    return num_trees


def part2(input_lines):
    full_map = load_map(input_lines)

    one_one = traverse(full_map, 1, 1)
    five_one = traverse(full_map, 5, 1)
    seven_one = traverse(full_map, 7, 1)
    one_two = traverse(full_map, 1, 2)

    num_trees = part1(input_lines) * one_one * five_one * seven_one * one_two
    print("Part 2 = {} trees encountered".format(num_trees))


if __name__ == '__main__':
    with open('../resources/day_3_input.txt', mode="r") as file:
        in_lines = list(file)

    part1(in_lines)
    part2(in_lines)
