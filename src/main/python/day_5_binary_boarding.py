#!/usr/bin/env python3


def part1(input_lines):
    max_seat_id = 0

    all_seat_ids = []

    for boarding_pass in input_lines:
        row_code = boarding_pass[0:7]
        column_code = boarding_pass[7:10]

        row_code = row_code.replace("F", "0")
        row_code = row_code.replace("B", "1")
        column_code = column_code.replace("L", "0")
        column_code = column_code.replace("R", "1")

        row_num = int(row_code, 2)
        column_num = int(column_code, 2)
        seat_id = row_num * 8 + column_num

        max_seat_id = max(max_seat_id, seat_id)
        all_seat_ids.append(seat_id)

    print("Part 1 => max seat ID = {} ".format(max_seat_id))

    return sorted(all_seat_ids)


def part2(input_lines):
    all_seat_ids = part1(input_lines)
    for seat_id in range(min(all_seat_ids), max(all_seat_ids)):
        to_check = seat_id + 1
        if to_check not in all_seat_ids:
            print("Part 2 => missing seat ID = {} ".format(to_check))


if __name__ == '__main__':
    with open("../resources/day_5_input.txt", mode='r') as file:
        lines = list(file)

    part1(lines)
    part2(lines)
