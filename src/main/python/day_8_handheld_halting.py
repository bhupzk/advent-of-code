#!/usr/bin/env python3


def part1(input_lines):

    accumulator = 0
    visited = set()                         # indexes of line #'s visited

    i = 0
    while i < len(input_lines):
        line = input_lines[i]
        operation, arg = line.split()

        if i in visited:
            return True, accumulator       # visited before => infinite loop, so break out, indicating a loop was found
        else:
            visited.add(i)

        if operation == "acc":
            accumulator += int(arg)
        if operation == "jmp":
            i += int(arg)
            continue                        # avoids incrementing "i" again below

        i += 1

    # Completed without finding any loops; return accumulator + False (indicating no loop found)
    return False, accumulator


def part2(input_lines):
    i = 0
    while i < len(input_lines):
        line = input_lines[i]
        operation, arg = line.split()

        if operation == "jmp":
            result = _try_swapping(input_lines, i, "jmp", "nop", arg)
            if result:
                return result

        elif operation == "nop":
            result = _try_swapping(input_lines, i, "nop", "jmp", arg)
            if result:
                return result

        i += 1


def _try_swapping(input_lines, line_num, orig_value, new_value, arg):
    input_lines[line_num] = "{} {}".format(new_value, arg)
    loop_found, acc = part1(input_lines)
    if not loop_found:
        # if no loops found, return the accumulator found
        return acc
    else:
        # otherwise, if a loop was found, restore original instruction value and carry on
        input_lines[line_num] = "{} {}".format(orig_value, arg)


if __name__ == '__main__':
    with open("../resources/day_8_input.txt", mode='r') as file:
        lines = list(file)

    print("Part 1 = {}".format(part1(lines)))
    print("Part 2 = {}".format(part2(lines)))
