#!/usr/bin/env python3

import sys
import math


def part1(start_time, bus_times):
    next_wait = sys.maxsize
    next_bus_id = sys.maxsize

    for bus_id in bus_times:
        if bus_id != "x":
            last_bus_num = math.floor(start_time/int(bus_id))
            next_time = int(bus_id) * (last_bus_num + 1)

            if (next_time - start_time) < next_wait:
                next_wait = next_time - start_time
                next_bus_id = int(bus_id)

    return next_wait * next_bus_id


def part2(bus_times):
    tuples = []     # tuple of (index, bus_id) for all input != "x"

    for index, bus_id in enumerate(bus_times):
        if bus_id != "x":
            tuples.append((index, int(bus_id)))

    start = 0
    step = 1
    for index, bus_id in tuples:
        while (start + index) % bus_id != 0:
            start += step
        step *= bus_id

    return start


if __name__ == '__main__':
    with open("../resources/day_13_input.txt", mode="r") as file:
        lines = list(file)
        start_time = int(lines[0])
        bus_times = lines[1].split(",")

    print("Part 1 = {}".format(part1(start_time, bus_times)))
    print("Part 2 = {}".format(part2(bus_times)))
