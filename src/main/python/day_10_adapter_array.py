#!/usr/bin/env python3

def part1(input_lines):
    joltages = sorted(input_lines)
    joltages.insert(0, 0)                       # charging outlet has an effective rating of 0 jolts, so add this in
    num_1_diffs = 0
    num_3_diffs = 1                             # built-in adapter is always 3 higher than highest adapter
    for i in range(len(joltages)-1):
        diff = joltages[i+1] - joltages[i]
        if diff == 1:
            num_1_diffs += 1
        elif diff == 3:
            num_3_diffs += 1
    return num_1_diffs * num_3_diffs


def part2(input_lines):
    joltages = sorted(input_lines)
    joltages.append(joltages[-1] + 3)

    cache = {0: 1}
    for joltage in joltages:
        cache[joltage] = cache.get(joltage - 3, 0) + cache.get(joltage - 2, 0) + cache.get(joltage - 1, 0)

    return cache[joltages[-1]]


if __name__ == '__main__':
    with open("../resources/day_10_input.txt", mode="r") as file:
        lines = [int(line) for line in list(file)]

    print("Part 1 = {}".format(part1(lines)))
    print("Part 2 = {}".format(part2(lines)))
