#!/usr/bin/env python3


def part1(input_lines):
    total = 0

    for line in input_lines:
        line = line.replace("\n", "")
        unique_chars = set()
        for char in line:
            unique_chars.add(char)
        total += len(unique_chars)

    print("Part 1 = {}".format(total))


def part2(input_lines):
    total = 0

    for line in input_lines:
        num_people = 1
        char_to_count = {}
        for char in line:
            if char == "\n":
                num_people += 1
                continue
            char_count = char_to_count.get(char, 0) + 1
            char_to_count[char] = char_count

        total += sum(v == num_people for v in char_to_count.values())

    print("Part 2 = {}".format(total))


if __name__ == '__main__':
    with open("../resources/day_6_input.txt", mode='r') as file:
        in_lines = file.read()
        in_lines = in_lines.split('\n\n')

    part1(in_lines)
    part2(in_lines)
