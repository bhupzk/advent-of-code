#!/usr/bin/env python3

import re


def part1(input_lines):
    num_valid = 0

    keys_to_check = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']   # cid is optional so no need to check it

    for line in input_lines:
        line = line.replace("\n", " ")

        key_check_count = sum(1 for key in keys_to_check if key in line)

        if key_check_count == len(keys_to_check):
            num_valid += 1

    print("Part 1 = {} valid passports".format(num_valid))


def part2(input_lines):
    num_valid = 0

    keys_to_check = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']   # cid is optional so no need to check it

    for line in input_lines:
        key_check_count = 0
        line = line.replace("\n", " ")
        passport_fields = {}
        all_fields = line.split()
        for field in all_fields:
            field_name, field_value = field.split(':')
            passport_fields[field_name] = field_value

        for field_name, field_value in passport_fields.items():
            if field_name == 'byr' and 1920 <= int(field_value) <= 2002:
                key_check_count += 1
            if field_name == 'iyr' and 2010 <= int(field_value) <= 2020:
                key_check_count += 1
            if field_name == 'eyr' and 2020 <= int(field_value) <= 2030:
                key_check_count += 1
            if field_name == 'hgt':
                regex_match = re.match('^(\\d+)(cm|in)$', field_value)
                if regex_match:
                    if regex_match.group(2) == 'cm' and 150 <= int(regex_match.group(1)) <= 193:
                        key_check_count += 1
                    if regex_match.group(2) == 'in' and 59 <= int(regex_match.group(1)) <= 76:
                        key_check_count += 1
            if field_name == 'hcl' and re.match('^#([0-9]|[a-f]){6}$', field_value):
                key_check_count += 1
            if field_name == 'ecl' and field_value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
                key_check_count += 1
            if field_name == 'pid' and re.match('^[0-9]{9}$', field_value):
                key_check_count += 1

        if key_check_count == len(keys_to_check):
            num_valid += 1

    print("Part 2 = {} valid passports".format(num_valid))


if __name__ == '__main__':
    with open('../resources/day_4_input.txt', mode="r") as file:
        in_lines = file.read()
        in_lines = in_lines.split('\n\n')

    part1(in_lines)
    part2(in_lines)
