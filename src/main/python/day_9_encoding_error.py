#!/usr/bin/env python3


def part1(input_lines):
    for i in range(25, len(input_lines)):
        target = input_lines[i]
        nums_to_check = input_lines[i-25:i]
        if not two_sum(sorted(nums_to_check), target):
            return target


def part2(input_lines):
    target = part1(input_lines)
    window = []
    running_total = 0

    line_num = 25
    while line_num < len(input_lines):
        if running_total == target:
            return min(window) + max(window)
        elif running_total < target:
            next_num = input_lines[line_num]
            window.append(next_num)
            running_total += next_num
            line_num += 1
        else:
            prev_num = window.pop(0)
            running_total -= prev_num


def two_sum(nums, target):
    i = 0
    j = len(nums) - 1
    while i < j:
        sum = nums[i] + nums[j]
        if sum == target:
            return True
        elif sum > target:
            j -= 1
        else:
            i += 1
    return False


if __name__ == '__main__':
    with open("../resources/day_9_input.txt", mode="r") as file:
        lines = [int(line) for line in list(file)]

    print("Part 1 = {}".format(part1(lines)))
    print("Part 2 = {}".format(part2(lines)))
