#!/usr/bin/env python3

import re


def part1(input_lines):
    total_count = 0
    for line in input_lines:
        parts = re.split('-| |: ', line)
        min = int(parts[0])
        max = int(parts[1])
        char_to_check = parts[2]
        password = parts[3]

        count = 0
        for char in password:
            if char == char_to_check:
                count += 1

        if min <= count <= max:
            total_count += 1

    print("Part 1 = {} valid passwords".format(total_count))


def part2(input_lines):
    total_count = 0
    for line in input_lines:
        parts = re.split('-| |: ', line)
        pos1 = int(parts[0])
        pos2 = int(parts[1])
        char_to_check = parts[2]
        password = parts[3]

        count = 0
        # must -1 from indexes here as problem states not to use 0-indexing
        if password[pos1 - 1] == char_to_check:
            count += 1
        if password[pos2 - 1] == char_to_check:
            count += 1

        if count == 1:
            total_count += 1

    print("Part 2 = {} valid passwords".format(total_count))


if __name__ == '__main__':
    with open('../resources/day_2_input.txt', mode="r") as file:
        in_lines = list(file)

    part1(in_lines)
    part2(in_lines)
