#!/usr/bin/env python3

directions = ["N", "E", "S", "W"]


def part1(input_lines):
    x = 0
    y = 0
    current_direction = "E"

    for line in input_lines:
        action, value = line[:1], int(line[1:])

        if action in ("N", "S", "E", "W"):
            x, y = move(action, value, x, y)
        elif action == "F":
            x, y = move(current_direction, value, x, y)
        elif action == "L":
            index = directions.index(current_direction)
            new_index = (index - (value / 90)) % 4
            current_direction = directions[int(new_index)]
        elif action == "R":
            index = directions.index(current_direction)
            new_index = (index + (value / 90)) % 4
            current_direction = directions[int(new_index)]

    return abs(x) + abs(y)


def move(action, value, x, y):
    if action == "N":
        y += value
    elif action == "S":
        y -= value
    elif action == "E":
        x += value
    elif action == "W":
        x -= value
    return x, y


def part2(input_lines):
    x = 0
    y = 0
    waypoint_x = 10
    waypoint_y = 1

    for line in input_lines:
        action, value = line[:1], int(line[1:])

        if action in ("N", "S", "E", "W"):
            waypoint_x, waypoint_y = move(action, value, waypoint_x, waypoint_y)
        elif action == "F":
            x += value * waypoint_x
            y += value * waypoint_y
        elif action == "L":
            if value == 90:
                waypoint_x, waypoint_y = -waypoint_y, waypoint_x
            elif value == 180:
                waypoint_x, waypoint_y = -waypoint_x, -waypoint_y
            elif value == 270:
                waypoint_x, waypoint_y = waypoint_y, -waypoint_x
        elif action == "R":
            if value == 90:
                waypoint_x, waypoint_y = waypoint_y, -waypoint_x
            elif value == 180:
                waypoint_x, waypoint_y = -waypoint_x, -waypoint_y
            elif value == 270:
                waypoint_x, waypoint_y = -waypoint_y, waypoint_x

    return abs(x) + abs(y)


if __name__ == '__main__':
    with open("../resources/day_12_input.txt", mode="r") as file:
        lines = list(file)

    print("Part 1 = {}".format(part1(lines)))
    print("Part 2 = {}".format(part2(lines)))
