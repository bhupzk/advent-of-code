#!/usr/bin/env python3

def part1(input_list):
    expenses = sorted(input_list)

    a = 0
    b = len(expenses) - 1

    while a < b:
        val1 = expenses[a]
        val2 = expenses[b]
        sum = val1 + val2
        if sum == 2020:
            print("Part 1 = {}".format(val1 * val2))
            return
        elif sum < 2020:
            # sum too low - move 1st pointer to next highest
            a += 1
        else:
            # only possibility here is sum > 2020, so sum is too high - move 2nd pointer to next lowest
            b -= 1


def part2(input_list):
    # Disclaimer: I got really lazy at this point + distracted by Netflix! ;-D
    for i in input_list:
        for j in input_list:
            for k in input_list:
                if i + j + k == 2020:
                    print("Part 2 = {}".format(i * j * k))
                    return


if __name__ == '__main__':
    with open('../resources/day_1_input.txt', mode="r") as file:
        ints = []
        for s in file:
            ints.append(int(s))

    part1(ints)
    part2(ints)
