#!/usr/bin/env python3

import re
from collections import defaultdict


def part1(colour_to_find):
    results = set(child_to_parents[colour_to_find])

    for parent in child_to_parents[colour_to_find]:
        results.update(part1(parent))

    return results


def part2(colour_to_find):
    total = 0

    for child, count in parent_to_child_counts[colour_to_find].items():
        total += count + (count * part2(child))

    return total


if __name__ == '__main__':
    with open("../resources/day_7_input.txt", mode='r') as file:
        lines = list(file)

    child_to_parents = defaultdict(set)         # {child -> set(parents)}       -- for part 1
    parent_to_child_counts = defaultdict(dict)  # {parent -> {child -> count}}  -- for part 2

    for line in lines:
        parent, children = line.split(" bags contain ")

        for num, child in re.findall(r"(\d+) (.*?) bag", children):
            child_to_parents[child].add(parent)
            parent_to_child_counts[parent][child] = int(num)

    print("Part 1 = {}".format(len(part1("shiny gold"))))
    print("Part 2 = {}".format(part2("shiny gold")))
